require('./bootstrap');

window.Vue = require('vue');

import Treatment from "./components/Treatment";
//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const app = new Vue({
    el: '#app',
    render: h => h(Treatment)
});

<?php

namespace App\Traits\Eloquent;

use Illuminate\Support\Facades\Storage;

trait Uploadable
{
    /**
     * Сохранения файла
     * @param $file
     * @param string $storage
     * @param string $folder
     * @return |null
     */
    public function upload($file, $storage = 'public', $folder = 'uploads')
    {
        $filename = uniqid() . '_' . str_replace(' ', '_', $file->getClientOriginalName());
        $path = Storage::disk($storage)->putFileAs($folder, $file, $filename);
        $file->move($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR, $path);
        if (Storage::disk($storage)->exists($path)) {
            return $path;
        }
        return null;
    }
}

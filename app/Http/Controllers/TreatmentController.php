<?php

namespace App\Http\Controllers;

use App\Treatment;
use Dotenv\Validator;
use Illuminate\Http\Request;

class TreatmentController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $treatment = new Treatment();

        if ($treatment->validator($request->all())->validate()) {
            $treatment->name = $request->name;
            $treatment->phone = $request->phone;
            $treatment->treatments = $request->treatment;
            $treatment->save();
        }

        return response()->json([
            'message' => 'Данные успешно сохранены',
        ]);

    }
}
